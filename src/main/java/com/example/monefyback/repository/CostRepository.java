package com.example.monefyback.repository;

import com.example.monefyback.domain.Cost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.time.LocalDate;
import java.util.List;

public interface CostRepository extends JpaRepository<Cost, Long> {

    @Query(value = "SELECT c FROM Cost c WHERE  c.createdAt BETWEEN ?1 AND ?2")
    List<Cost> findByInterval(LocalDate startDate, LocalDate endDate);

    @Query(value = "select c from Cost c where c.createdAt = ?1")
    List<Cost> findByDate(LocalDate date);

    @Query(value = "select sum(c.cost) from Cost c ")
    Double getSumCost();

    @Query(value = "SELECT * FROM cost WHERE EXTRACT(MONTH FROM created_at) = ?1", nativeQuery = true)
    List<Cost> findByMonth(Integer monthNum);
}
