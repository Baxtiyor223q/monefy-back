package com.example.monefyback.domain.register;

import com.example.monefyback.domain.BaseDomain;
import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.persistence.Column;
import javax.persistence.Entity;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class Role extends BaseDomain {

    @Column(length = 60, nullable = false)
    private String roleName;

}
