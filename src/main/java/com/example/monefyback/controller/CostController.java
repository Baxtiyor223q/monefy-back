package com.example.monefyback.controller;

import com.example.monefyback.domain.Cost;
import com.example.monefyback.model.CalculusDto;
import com.example.monefyback.service.CostService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/cost")
@RequiredArgsConstructor
public class CostController {

    private final CostService costService;

    @PostMapping("/add-cost")
    private Double addCost(@RequestBody CalculusDto calculusDto){
        return costService.addCost(calculusDto);
    }

    @GetMapping("/get-by-interval")
    private List<Cost> getByInterval(@RequestParam String startDate, @RequestParam String endDate){
        return costService.interval(startDate, endDate);
    }
    @GetMapping("/get-by-date")
    private List<Cost> getByDate(@RequestParam String date){
        return costService.findByDate(date);
    }
    @GetMapping("/get-sum")
    private Double getSum(){
        return costService.getSumCost();
    }
    @GetMapping("/get-by-month")
    private List<Cost> getByMonth(@RequestParam Integer monthNum){
        return costService.getByMonth(monthNum);
    }

}
