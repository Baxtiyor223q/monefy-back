package com.example.monefyback.controller;

import com.example.monefyback.model.CategoryDto;
import com.example.monefyback.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/add-category")
    private String addCategory(@RequestBody CategoryDto categoryDto){
        return categoryService.addCategory(categoryDto);
    }
    @PutMapping("/update-category/{id}")
    private String updateCategory(@RequestBody CategoryDto categoryDto, @PathVariable("id") Long id){
        return categoryService.updateCategory(id, categoryDto);
    }
}
