package com.example.monefyback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonefyBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonefyBackApplication.class, args);
    }

}
