package com.example.monefyback.service;

import com.example.monefyback.domain.register.Role;
import com.example.monefyback.model.register.RoleDto;
import com.example.monefyback.repository.register.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository repository;

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();

    public Long addRole(RoleDto dto){
        Set<ConstraintViolation<RoleDto>> violations = validator.validate(dto);
        if (violations.size() > 1){
            StringBuilder stringBuilder = new StringBuilder();
            violations.forEach(validateError -> stringBuilder.append(validateError.getMessage()));
            throw new RuntimeException(stringBuilder.toString());
        }

      Role role = Role.builder()
              .roleName(dto.getRoleName())
              .createdAt(LocalDate.now())
              .build();
        return repository.save(role).getId();

    }
}
