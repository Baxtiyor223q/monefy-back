package com.example.monefyback.service;

import com.example.monefyback.domain.Category;
import com.example.monefyback.domain.register.User;
import com.example.monefyback.exception.NotFoundException;
import com.example.monefyback.exception.NotUpdatedException;
import com.example.monefyback.model.CategoryDto;
import com.example.monefyback.repository.CategoryRepository;
import com.example.monefyback.repository.register.UserRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final ObjectMapper objectMapper;
    private final UserRepository userRepository;

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();

    public String addCategory(CategoryDto categoryDto){
        Set<ConstraintViolation<CategoryDto>>  violations = validator.validate(categoryDto);
        if (violations.size() > 1 ){
            StringBuilder stringBuilder = new StringBuilder();
            violations.forEach(validationError -> stringBuilder.append(validationError.getMessage()));
            throw new RuntimeException(stringBuilder.toString());
        }
        Optional<User> userOptional = userRepository.findById(categoryDto.getUserId());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            Category category = Category.builder()
                    .categoryName(categoryDto.getCategoryName())
                    .createdAt(LocalDate.now())
                    .user(user)
                    .build();
            categoryRepository.save(category);
            return category.getCategoryName();
        }
        throw new RuntimeException(String.format("Could not found [user] by id%s", categoryDto.getUserId()));
    }
    public String updateCategory(Long id, CategoryDto categoryDto){
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (categoryOptional.isPresent()){
            Category categoryUpdate = categoryOptional.get();
            try {
                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                objectMapper.updateValue(categoryUpdate, categoryDto);

            } catch (JsonMappingException e) {
                throw new NotUpdatedException("Could not update!");
            }
            categoryRepository.save(categoryUpdate);
            return  categoryUpdate.getCategoryName();
        }
        throw new NotFoundException(String.format("Could not found category by %s id", id));
    }
}
