package com.example.monefyback.service;

import com.example.monefyback.domain.Category;
import com.example.monefyback.domain.Income;
import com.example.monefyback.exception.NotFoundException;
import com.example.monefyback.model.CalculusDto;
import com.example.monefyback.repository.CategoryRepository;
import com.example.monefyback.repository.IncomeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class IncomeService {

    private final IncomeRepository incomeRepository;
    private final CategoryRepository categoryRepository;
    private final ObjectMapper objectMapper;

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();

    public Double addIncome(CalculusDto calculusDto){
        Set<ConstraintViolation<CalculusDto>> violations = validator.validate(calculusDto);
        if (violations.size() > 1){
            StringBuilder stringBuilder = new StringBuilder();
            violations.forEach(validateError -> stringBuilder.append(validateError.getMessage()));
            throw new RuntimeException(stringBuilder.toString());
        }
        Optional<Category> categoryOptional = categoryRepository.findById(calculusDto.getCategoryId());
        if (categoryOptional.isPresent()){
            Category category = categoryOptional.get();
            Income income = Income.builder()
                    .income(calculusDto.getCalculus())
                    .createdAt(LocalDate.now())
                    .note(calculusDto.getNote())
                    .category(category)
                    .build();
            return incomeRepository.save(income).getIncome();
        }
        throw new NotFoundException(String.format("Could not found category by %s id", calculusDto.getCategoryId()));
    }
        public List<Income> interval(String startDate, String endDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startLocalDate = LocalDate.parse((startDate), formatter);
        LocalDate endLocalDate = LocalDate.parse((endDate), formatter);
        return objectMapper.convertValue(incomeRepository.findByInterval(startLocalDate, endLocalDate), new TypeReference<>() {
        });
    }
    public List<Income> findByDate(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse((date), formatter);
        return objectMapper.convertValue(incomeRepository.findByDate(localDate), new TypeReference<>() {
        });
    }
    public Double getSumIncome(){
        return incomeRepository.getSumIncome();
    }
    public List<Income> getByMonth(Integer monthNum){
        return objectMapper.convertValue(incomeRepository.findByMonth(monthNum), new TypeReference<>() {
        });
    }

}
