package com.example.monefyback.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@AllArgsConstructor
public class CategoryDto implements Serializable {

     @NotNull(message = "Column cannot be null!")
     String categoryName;

     @NotNull(message = "Column cannot be null!")
     Long userId;
}
