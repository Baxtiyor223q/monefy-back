package com.example.monefyback.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CalculusDto implements Serializable {

    @NotNull(message = "Column cannot be null!")
    Double calculus;

    @NotNull(message = "Column cannot be null!")
    Long categoryId;

    String note;
}
